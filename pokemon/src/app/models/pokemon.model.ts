export interface Pokemon {
    name: string,
    url: string,
    id: number,
    img: string,
    collected: boolean
}