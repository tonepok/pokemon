import { Pokemon } from './pokemon.model'
export interface PokemonMidl {
    count: number,
    next: String,
    previous: String,
    results: Pokemon[]
}