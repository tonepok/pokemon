import { Component } from "@angular/core";
import {Router} from '@angular/router';

@Component({
    selector: 'app-user',
    templateUrl: './user.page.html',
}) 
export class UserPage{ 
    constructor(private readonly router: Router) {
    }

    ngOnInit(): void{
        let name = localStorage.getItem('name');
        if (name == null){
            this.router.navigate(['login'])
        }
    }

}