import { Component, OnInit } from "@angular/core";
import {Router} from '@angular/router';

@Component({
    selector: 'app-pokemon',
    templateUrl: './pokemon.page.html',
}) 
export class PokemonPage implements OnInit{
    
    constructor(private readonly router: Router) {
    }

    ngOnInit(): void{
        let name = localStorage.getItem('name');
        if (name == null){
            this.router.navigate(['login'])
        }
    }

}