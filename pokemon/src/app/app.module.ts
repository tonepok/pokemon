import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { CatalogueComponent } from './catalogue/catalogue.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LoginComponent } from './login/login.component';
import { PokemonPage } from './pokemon/pokemon.page';
import { AppRoutingModule } from './app-routing.module'
import { WelcomePage } from './welcome/welcome.page';
import { UserPage } from './user/user.page';
import { TrainerComponent } from './trainer/trainer.component';

@NgModule({
  declarations: [
    AppComponent,
    CatalogueComponent,
    NavbarComponent,
    LoginComponent,
    TrainerComponent,
    PokemonPage,
    WelcomePage,
    UserPage
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
