import { Component, Input } from "@angular/core";
import { Pokemon } from "../models/pokemon.model";
import {Router} from '@angular/router';

@Component({
    selector: 'app-trainer',
    templateUrl: './trainer.component.html',
    styleUrls: ['./trainer.component.css']
}) 
export class TrainerComponent{
    constructor(private readonly router: Router){
        
    }

    get pokemon(): Pokemon[] {
        const local = localStorage.getItem("collected");
        if (local){
            const newLocal = local+"]";
            let pokelist = JSON.parse(newLocal);
            return pokelist;
        }
        return [];
    }

    

}