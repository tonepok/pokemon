import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PokemonPage } from "./pokemon/pokemon.page";
import { WelcomePage } from "./welcome/welcome.page";
import { UserPage } from "./user/user.page";

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: '/login'
    },
    {
        path: 'pokemon',
        component: PokemonPage,
        data: {title: 'Pokemon Catalogue'}
    },
    {
        path: 'login',
        component: WelcomePage,
        data: {title: 'Login Page'}
    },
    {
        path: 'trainer',
        component: UserPage,
        data: {title: 'Trainer Page'}
    },
]

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}