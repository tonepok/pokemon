import { Injectable } from '@angular/core'
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Pokemon } from '../models/pokemon.model'
import { PokemonMidl } from '../models/pokemonmidl.model'

@Injectable({
    providedIn: 'root'
})
export class CatalogueService {
    private _catalogue: Pokemon[] = [];
    private _error: string = '';

    constructor(private readonly http: HttpClient) {
    }

    //fetching from the API and adding some properties. Had to make another model "PokemonMidl" to handle
    //that it was the result-part that is going to be a Pokemon.
    public fetchPokemon(): void {
        this.http.get<PokemonMidl>('https://pokeapi.co/api/v2/pokemon?limit=151') //first gen
        .subscribe((catalogue: PokemonMidl) => {
            this._catalogue = catalogue.results;
            for (let i in this._catalogue){
                this._catalogue[i].id=+i
                let e=+i+1;
                this._catalogue[i].img="https://raw.githubusercontent.com/PokeAPI/sprites/84204f8594790cfd04190a8d82beb31f49115c02/sprites/pokemon/other/dream-world/"+(e)+".svg"
                this._catalogue[i].collected=false;
            }
        }, (error: HttpErrorResponse) => {
            this._error = error.message;
        });
    }

    public pokemon(): Pokemon[] {
        return this._catalogue;
    }

    public error(): string {
        return this._error;
    }
}