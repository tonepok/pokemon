import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Data, Router } from '@angular/router';  

@Component({    
    selector:'app-navbar',    
    templateUrl:'./navbar.component.html',    
    styleUrls:['./navbar.component.css']    
    }) 
export class NavbarComponent{    
    pageTitle = 'default page title';
    constructor(private route: ActivatedRoute, private readonly router: Router) {}

    //method to change what text is hown in the navbar
    ngOnInit() {
      this.route.data.subscribe(
        (data: Data) => {
          this.pageTitle = data['title'];
        }
      );
    }
    
    //log out and navigate back to the login section
    public logout(){
      localStorage.clear();
      this.router.navigate(['login'])
    }
}   