import { Component, OnInit } from "@angular/core";
import { Pokemon } from "../models/pokemon.model";
import { CatalogueService } from "../services/catalogue.service";

@Component({
    selector: 'app-catalogue',
    templateUrl: './catalogue.component.html',
    styleUrls: ['./catalogue.component.css']
}) 
export class CatalogueComponent implements OnInit{

    constructor(private readonly catalogueService: CatalogueService){
    } 

    ngOnInit(): void{
        this.catalogueService.fetchPokemon();
    }

    //i am storing the collected pokemon in a string in the local storage so that the ones
    //that are collected is not overwritten when I get the pokemon
    get pokemon(): Pokemon[] {
        let pok: Pokemon[] =  this.catalogueService.pokemon();
        let str = localStorage.getItem("everyone")
        if (str){
            for (let i = 0; i < pok.length; i++) {
                if (str.charAt(i) == 'x'){
                    pok[i].collected=true;
                }
            }
        }

        return pok;
    }

    //triggered when u click on a pokemon.
    public collect(item: Pokemon){
        if(!item.collected){
            item.collected=true;
            let local = localStorage.getItem("collected");
            let newLocal = "";
            if (local!=null){
                if(local.length > 2){
                    newLocal = local+=", "; 
                }
                else {
                    newLocal = local+="["; 
                }
            }
            this.updateArray(item.id);
            newLocal = newLocal+=(JSON.stringify(item));
            localStorage.setItem("collected", newLocal);
        }
    }

    //updates the local storage containing the collected pokemon.
    //i compare the indexes to know which are collected
    private updateArray(id: number){
        let str = localStorage.getItem("everyone")
        if (str){
            let newStr = str.substring(0, id) + 'x' + str.substring(id + 1);
            localStorage.setItem("everyone", newStr)
        }
  
    }
}