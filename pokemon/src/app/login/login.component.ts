import { Component } from "@angular/core";
import { NgForm } from "@angular/forms";
import {Router} from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
}) 
export class LoginComponent{
    constructor(private readonly router: Router) {
    }
  
    name: string = '';

    //initializing the local storage when the user logs in
    public onSubmit(form:NgForm) {
        this.name=form.value.title;
        localStorage.setItem('name', this.name);
        localStorage.setItem('collected', "");
        let newArr = "0"
        newArr.repeat(251);
        for (let i=0; i<251; i++){
            newArr+="0"
        }
        localStorage.setItem("everyone", newArr)
        this.router.navigate(['pokemon'])
    }

}